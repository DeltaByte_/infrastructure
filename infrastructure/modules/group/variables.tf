terraform {
  required_providers {
    gitlab = { source = "gitlabhq/gitlab" }
  }
}

variable "name" {
  type = string
}

variable "namespace" {
  type = string
}

variable "visibility" {
  type    = string
  default = "public"
}

variable "variables" {
  type = map(object({
    value             = string
    masked            = optional(bool, false)
    protected         = optional(bool, false)
    environment_scope = optional(string, "*")
  }))
}
