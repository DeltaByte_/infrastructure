resource "gitlab_project_environment" "env" {
  for_each = var.environments

  project      = gitlab_project.main.id
  name         = each.key
  external_url = each.value.url

  lifecycle {
    ignore_changes = [
      external_url
    ]
  }
}

resource "gitlab_project_protected_environment" "access_level" {
  # only create resource if access_level is set
  for_each = { for key, value in var.environments : key => value if value.access_level != "" }

  project     = gitlab_project_environment.env[each.key].project
  environment = gitlab_project_environment.env[each.key].name

  required_approval_count = 1

  deploy_access_levels {
    access_level = each.value.access_level
  }
}