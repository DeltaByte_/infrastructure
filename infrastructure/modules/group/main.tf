resource "gitlab_group" "main" {
  name             = var.name
  path             = replace(lower(var.name), "/\\W/", "-")
  parent_id        = var.namespace
  visibility_level = var.visibility
}

resource "gitlab_group_variable" "default" {
  for_each          = var.variables
  group             = gitlab_group.main.id
  key               = each.key
  value             = each.value.value
  protected         = each.value.protected
  masked            = each.value.masked
  environment_scope = each.value.environment_scope
}
