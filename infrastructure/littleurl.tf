locals {
  littleurl_environments = {
    "dev"  = {},
    "prod" = { access_level = "maintainer" }
  }
}

# ----------------------------------------------------------------------------------------------------------------------
# Root
# ----------------------------------------------------------------------------------------------------------------------
module "littleurl_remotestate" {
  source = "./modules/remote-state"

  application  = "littleurl"
  environments = [for env, conf in local.littleurl_environments : env]
  aws_accounts = var.aws_accounts_littleurl
}

module "littleurl_group_root" {
  source = "./modules/group"

  name      = "LittleURL"
  namespace = var.gitlab_root_namespace

  variables = {
    "TF_STATE_BUCKET" = {
      value  = module.littleurl_remotestate.state_bucket
      masked = true
    },
    "TF_STATE_TABLE" = {
      value  = module.littleurl_remotestate.state_table
      masked = true
    }
  }
}

# ----------------------------------------------------------------------------------------------------------------------
# Projects
# ----------------------------------------------------------------------------------------------------------------------
module "litturl_project_api" {
  source = "./modules/project"

  name         = "api"
  description  = "LittleURL API"
  namespace    = module.littleurl_group_root.group_id
  environments = local.littleurl_environments
}

module "litturl_project_dashboard" {
  source = "./modules/project"

  name        = "dashboard"
  description = "LittleURL Dashboard GUI"
  namespace   = module.littleurl_group_root.group_id
}
