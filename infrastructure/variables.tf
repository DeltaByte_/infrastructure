# ----------------------------------------------------------------------------------------------------------------------
# Misc
# ----------------------------------------------------------------------------------------------------------------------
variable "application" {
  type    = string
  default = "littleurl"
}

variable "environments" {
  type    = set(string)
  default = ["dev", "prod"]
}

# ----------------------------------------------------------------------------------------------------------------------
# AWS
# ----------------------------------------------------------------------------------------------------------------------
locals {
  aws_org_role = "OrganizationAccountAccessRole"
}

variable "aws_region" {
  type    = string
  default = "us-east-1"
}

variable "aws_default_tags" {
  type        = map(string)
  description = "Common resource tags for all AWS resources"
  default = {
    service = "infrastructure"
    project = "N/A"
  }
}

# ----------------------------------------------------------------------------------------------------------------------
# GitLab
# ----------------------------------------------------------------------------------------------------------------------
variable "gitlab_root_namespace" {
  type        = string
  description = "Gitlab project hierarchy"
  sensitive   = true
}

# ----------------------------------------------------------------------------------------------------------------------
# AWS Account IDs
# ----------------------------------------------------------------------------------------------------------------------
variable "aws_accounts_infrastructure" {
  type = object({
    prod = string
  })
}

variable "aws_accounts_littleurl" {
  type = object({
    prod = string
    dev  = string
  })
}
