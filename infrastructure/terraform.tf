terraform {
  backend "http" {}

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.61"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 15.10"
    }
  }
}

# ----------------------------------------------------------------------------------------------------------------------
# Providers
# ----------------------------------------------------------------------------------------------------------------------
provider "aws" {
  region = var.aws_region

  default_tags { tags = var.aws_default_tags }
}

provider "gitlab" {}
