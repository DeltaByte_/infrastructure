resource "aws_dynamodb_table" "remotestate_lock" {
  name         = "terraform-lock-${var.application}"
  hash_key     = "LockID"
  billing_mode = "PAY_PER_REQUEST"

  server_side_encryption {
    enabled = true
  }

  attribute {
    name = "LockID"
    type = "S"
  }
}
