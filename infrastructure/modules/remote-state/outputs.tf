output "state_bucket" {
  value = aws_s3_bucket.remotestate.id
}

output "state_table" {
  value = aws_dynamodb_table.remotestate_lock.name
}