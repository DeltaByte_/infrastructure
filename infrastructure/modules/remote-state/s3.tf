# ----------------------------------------------------------------------------------------------------------------------
# State storage
# ----------------------------------------------------------------------------------------------------------------------
resource "aws_s3_bucket" "remotestate" {
  bucket_prefix = "terraform-state-${var.application}"
}

resource "aws_s3_bucket_acl" "state" {
  bucket = aws_s3_bucket.remotestate.id
  acl    = "private"
}

resource "aws_s3_bucket_versioning" "remotestate" {
  bucket = aws_s3_bucket.remotestate.id
  versioning_configuration {
    status = "Enabled"
  }
}

# remove plans after 30 days
resource "aws_s3_bucket_lifecycle_configuration" "remotestate_plan" {
  bucket = aws_s3_bucket.remotestate.id
  rule {
    id     = "plan"
    status = "Enabled"

    filter {
      prefix = "plan/"
    }

    expiration {
      days = 30
    }
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "remotestate" {
  bucket = aws_s3_bucket.remotestate.bucket
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

# ----------------------------------------------------------------------------------------------------------------------
# State storage access
# ----------------------------------------------------------------------------------------------------------------------
resource "aws_s3_bucket_public_access_block" "remotestate" {
  bucket                  = aws_s3_bucket.remotestate.id
  block_public_acls       = true
  ignore_public_acls      = true
  block_public_policy     = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_policy" "remotestate" {
  depends_on = [aws_s3_bucket.remotestate]
  bucket     = aws_s3_bucket.remotestate.id
  policy     = data.aws_iam_policy_document.remotestate_bucket.json
}

data "aws_iam_policy_document" "remotestate_bucket" {
  statement {
    sid = "DenyIncorrectEncryptionHeader"

    effect = "Deny"

    principals {
      identifiers = ["*"]
      type        = "AWS"
    }

    actions = [
      "s3:PutObject"
    ]

    resources = [
      "${aws_s3_bucket.remotestate.arn}/*"
    ]

    condition {
      test     = "StringNotEquals"
      variable = "s3:x-amz-server-side-encryption"

      values = [
        "AES256"
      ]
    }
  }

  statement {
    sid = "DenyUnEncryptedObjectUploads"

    effect = "Deny"

    principals {
      identifiers = ["*"]
      type        = "AWS"
    }

    actions = [
      "s3:PutObject",
    ]

    resources = [
      "${aws_s3_bucket.remotestate.arn}/*"
    ]

    condition {
      test     = "Null"
      variable = "s3:x-amz-server-side-encryption"

      values = [
        "true"
      ]
    }
  }
}
