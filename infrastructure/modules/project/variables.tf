terraform {
  required_providers {
    gitlab = { source = "gitlabhq/gitlab" }
  }
}

variable "name" {
  type = string
}

variable "description" {
  type = string
}

variable "environments" {
  type = map(object({
    url          = optional(string, null)
    access_level = optional(string, "")
  }))

  default = {
    "dev"  = {},
    "prod" = {}
  }
}

variable "visibility" {
  type    = string
  default = "public"
}

variable "namespace" {
  type = string
}

variable "ci_config_path" {
  type    = string
  default = ".gitlab-ci/main.yml"
}
