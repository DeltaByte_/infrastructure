resource "aws_iam_role" "remotestate" {
  for_each = var.environments

  name               = "${title(var.application)}TerraformRemotestate${title(each.key)}"
  assume_role_policy = data.aws_iam_policy_document.remotestate_assume[each.key].json
  path               = "/remotestate/"

  tags = {
    environment = each.key
  }
}

data "aws_iam_policy_document" "remotestate_assume" {
  for_each = var.environments

  statement {
    actions = ["sts:AssumeRole", "sts:TagSession"]

    principals {
      type = "AWS"
      identifiers = [
        "arn:aws:iam::${var.aws_accounts[each.key]}:root"
      ]
    }

    condition {
      test     = "Null"
      variable = "aws:PrincipalTag/service"
      values   = [false]
    }
  }
}

resource "aws_iam_role_policy" "remotestate_access" {
  for_each = var.environments

  name   = "TerraformBackendAccess"
  role   = aws_iam_role.remotestate[each.key].id
  policy = data.aws_iam_policy_document.remotestate_access[each.key].json
}

data "aws_iam_policy_document" "remotestate_access" {
  for_each = var.environments

  statement {
    sid = "DynamoLock"

    actions = [
      "dynamodb:GetItem",
      "dynamodb:PutItem",
      "dynamodb:DeleteItem"
    ]

    resources = [aws_dynamodb_table.remotestate_lock.arn]
  }

  statement {
    sid = "S3ListObjects"

    actions = [
      "s3:ListBucket"
    ]

    resources = [aws_s3_bucket.remotestate.arn]
  }

  statement {
    sid = "S3GetAndPutObjects"

    actions = [
      "s3:PutObject",
      "s3:GetObject"
    ]

    resources = [
      "${aws_s3_bucket.remotestate.arn}/state/${each.key}/$${aws:PrincipalTag/service, 'INVALID_SERVICE_TAG'}.tfstate",
      "${aws_s3_bucket.remotestate.arn}/plan/${each.key}/$${aws:PrincipalTag/service, 'INVALID_SERVICE_TAG'}.tfplan"
    ]
  }
}
