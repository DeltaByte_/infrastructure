module "infrastructure_remotestate" {
  source = "./modules/remote-state"

  application  = "infrastructure"
  environments = ["prod"]
  aws_accounts = var.aws_accounts_infrastructure
}

module "infrastructure_project" {
  source = "./modules/project"

  name         = "Infrastructure"
  description  = "General infrasinfrstructure that sits outside of any specific project"
  namespace    = var.gitlab_root_namespace
  environments = { prod = {} }
}
