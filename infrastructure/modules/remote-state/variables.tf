variable "application" {
  type = string
}

variable "environments" {
  type    = set(string)
  default = ["dev", "prod"]
}

variable "aws_accounts" {
  type = map(string)
}