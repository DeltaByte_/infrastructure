resource "gitlab_project" "main" {
  name         = var.name
  path         = replace(lower(var.name), "/\\W/", "-")
  description  = var.description
  namespace_id = var.namespace

  # repo
  default_branch                                   = "main"
  visibility_level                                 = var.visibility
  archive_on_destroy                               = true
  allow_merge_on_skipped_pipeline                  = false
  issues_enabled                                   = true
  merge_method                                     = "merge"
  merge_requests_enabled                           = true
  merge_pipelines_enabled                          = true
  merge_trains_enabled                             = true
  only_allow_merge_if_all_discussions_are_resolved = true
  only_allow_merge_if_pipeline_succeeds            = true
  initialize_with_readme                           = false

  # ci
  ci_config_path              = var.ci_config_path
  ci_separated_caches         = true
  public_builds               = false
  infrastructure_access_level = "private"
}

resource "gitlab_branch_protection" "main" {
  project = gitlab_project.main.id
  branch  = gitlab_project.main.default_branch

  push_access_level  = "maintainer"
  merge_access_level = "developer"
  allow_force_push   = false
}
